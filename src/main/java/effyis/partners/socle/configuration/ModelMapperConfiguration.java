package effyis.partners.socle.configuration;
/*
 *   @author ayoubbenyas
 */


import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfiguration {
    @Bean
    ModelMapper getModelMapper(){
       ModelMapper modelMapper= new ModelMapper();
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        /*
        modelMapper.createTypeMap(ClientDTO.class, Client.class)
                .addMappings(mapper -> {
                    mapper.skip(Client::setId);
                });

        modelMapper.createTypeMap(ProductTypeDTO.class, ProductType.class)
                .addMappings(mapper -> {
                    mapper.skip(ProductType::setId);
                });

        modelMapper.createTypeMap(ProductDTO.class, Product.class)
                .addMappings(mapper -> {
                    mapper.skip(Product::setId);
                });

        modelMapper.createTypeMap(ServiceTypeDTO.class, ServiceType.class)
                .addMappings(mapper -> {
                    mapper.skip(ServiceType::setId);
                });

        modelMapper.createTypeMap(ServiceDTO.class, Service.class)
                .addMappings(mapper -> {
                    mapper.skip(Service::setId);
                });



        modelMapper.createTypeMap(ChannelTypeDTO.class, ChannelType.class)
                .addMappings(mapper -> {
                    mapper.skip(ChannelType::setId);
                });

        modelMapper.createTypeMap(ChannelDTO.class, Channel.class)
                .addMappings(mapper -> {
                    mapper.skip(Channel::setId);
                });


        modelMapper.createTypeMap(FeeTypeDTO.class, FeeType.class)
                .addMappings(mapper -> {
                    mapper.skip(FeeType::setId);
                });

        modelMapper.createTypeMap(FeeDTO.class, Fee.class)
                .addMappings(mapper -> {
                    mapper.skip(Fee::setId);
                });

        modelMapper.createTypeMap(LimitTypeDTO.class, LimitType.class)
                .addMappings(mapper -> {
                    mapper.skip(LimitType::setId);
                });


        modelMapper.createTypeMap(LimitDTO.class, Limit.class)
                .addMappings(mapper -> {
                    mapper.skip(Limit::setId);
                });

         */

        modelMapper.getConfiguration().setAmbiguityIgnored(true) ;
        //modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper;
    }
}
