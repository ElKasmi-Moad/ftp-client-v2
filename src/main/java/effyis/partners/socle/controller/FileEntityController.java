package effyis.partners.socle.controller;

import effyis.partners.socle.entity.FileEntity;
import effyis.partners.socle.service.FileEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/attachment")
public class FileEntityController {

    @Autowired
    FileEntityService fileEntityService;

    @PostMapping("/saveFile")
    public   String saveFile(@RequestBody FileEntity fileEntity){
        fileEntityService.saveFile(fileEntity);
        return "File Saved ! ";
    }

    @DeleteMapping("/deleteAll")
    public String deleteFile(){
        fileEntityService.deleteAll();
        return "All Files deleted";
    }
}
