package effyis.partners.socle.controller;

import effyis.partners.socle.service.FileUploadService;
import effyis.partners.socle.service.IFileService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;


@RestController
@RequestMapping("/ftp")
public class FileUploadController {

    @Autowired
    FileUploadService fileUploadService;


    public FileUploadController() {
    }
    /*  @PostMapping ("/uploadV2")
      @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
      public String uploadV2(@RequestParam("file") MultipartFile file) throws Exception{
          String message  =  ftpFileWriter.uploadOrder(file);
          return message;
      }
     */
    @GetMapping("/uploadV2/{fileName}")
    public String uploadV2(@PathVariable String fileName) {
        String message  =  fileUploadService.uploadOrder(fileName);
        return message;
    }

    @PostMapping ("/uploadV2")
    public String uploadV2(@RequestParam("file") MultipartFile file) throws Exception{
        //Path path = Paths.get(uploadDir + file.getOriginalFilename());
        String currentDirectory = System.getProperty("user.dir");


        return currentDirectory;
    }
    @PostMapping(value = "/example1/upload/file",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE} )
    public ResponseEntity<String> uploadSingleFileExample1(@RequestParam("file") MultipartFile file)  {
     String message =    fileUploadService.uploadFile(file);
        // Add your processing logic here
        return ResponseEntity.ok("Success" + message);
    }
    @GetMapping("/connect")
    public String connectToFTPServer(){
        if(fileUploadService.isConnected()){
            return "User already connected";
        }
         else {
            fileUploadService.open();
            return "User is Connected";
        }
    }
}
