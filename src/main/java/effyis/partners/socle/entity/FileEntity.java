package effyis.partners.socle.entity;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "file")
public class FileEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private  String type;

    private Long userId;

    private String fileName;


    public FileEntity(String type, Long userId, String fileName) {
        this.type = type;
        this.userId = userId;
        this.fileName = fileName;
    }

    public FileEntity() {
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
