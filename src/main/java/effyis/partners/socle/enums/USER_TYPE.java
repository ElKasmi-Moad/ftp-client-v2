package effyis.partners.socle.enums;
/*
 *   @author ayoubbenyas
 */

public enum USER_TYPE {
    BACK_END_API,
    BACKOFFICE,
    DATABASE,
}
