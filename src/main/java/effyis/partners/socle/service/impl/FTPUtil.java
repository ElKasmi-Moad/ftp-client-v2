package effyis.partners.socle.service.impl;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;


public class FTPUtil {

    private static Logger logger = LoggerFactory.getLogger(FTPUtil.class);

    FTPUtil(String ip,int port,String user,String pwd){
        this.ip = ip;
        this.port = port;
        this.pwd = pwd;
    }


    /**
     * Methods of uploading files exposed to the outside world
     * @param fileList
     * @return
     */
    public static boolean  uploadFile(List<File> fileList) throws IOException {
        FTPUtil ftpUtil = new FTPUtil("197.230.162.125",21,"nahid","Effyis-2020");
        logger.info("Start connection FTP The server");
        //Throw the exception to the service layer and do not handle it here
        boolean result = ftpUtil.uploadFile("img",fileList);
        logger.info("Start connection FTP Server, end upload, upload results{}",result);
        return result;
    }

    private boolean uploadFile(String remotePath,List<File> fileList) throws IOException {
        boolean uploaded = true;
        FileInputStream fis = null;
        //Connect to FTP server
        if(connectServer()){
            try {
                ftpClient.changeWorkingDirectory(remotePath);
                ftpClient.setBufferSize(1024);
                ftpClient.setControlEncoding("UTF-8");
                //Set to binary format to prevent garbled code
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                //Passive mode storage
                ftpClient.enterLocalPassiveMode();
                //Traverse file store
                for(File fileItem : fileList){
                    //Convert files to streams
                    fis = new FileInputStream(fileItem);
                    //Call storeFile method to store
                    ftpClient.storeFile(fileItem.getName(),fis);
                }
            } catch (IOException e) {
                logger.error("Upload file exception",e);
                uploaded = false;

            }
            finally {
                //Close connections and FileStream
                fis.close();
                ftpClient.disconnect();


            }
        }
        return uploaded;

    }


    private boolean connectServer( ){
        boolean isSuccess = false;
        ftpClient = new FTPClient();
        try{
            ftpClient.connect("197.230.162.125");
            isSuccess =  ftpClient.login("nahid","Effyis-2020");
        }catch (IOException e){
            logger.error("FTP Server connection failed",e);

        }
        return isSuccess;
    }
    private String ip ;
    private int port;
    private String user;
    private String pwd;
    private FTPClient ftpClient ;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }



    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public FTPClient getFtpClient() {
        return ftpClient;
    }

    public void setFtpClient(FTPClient ftpClient) {
        this.ftpClient = ftpClient;
    }







}
