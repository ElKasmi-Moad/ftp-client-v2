package effyis.partners.socle.service.impl;

import effyis.partners.socle.entity.FileEntity;
import effyis.partners.socle.repository.FileRepository;
import effyis.partners.socle.service.FileEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileEntityServiceImpl implements FileEntityService {

    @Autowired
    FileRepository fileRepository;

    @Override
    public void saveFile(FileEntity fileEntity) {
        fileRepository.save(fileEntity);
    }
    @Override
    public void deleteAll(){
        fileRepository.deleteAll();
    }
}
