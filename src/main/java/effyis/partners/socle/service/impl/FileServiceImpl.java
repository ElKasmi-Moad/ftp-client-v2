package effyis.partners.socle.service.impl;


import effyis.partners.socle.service.IFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by xiao on 2018/1/21.
 */
@Service("iFileService")
public class FileServiceImpl implements IFileService {

    //Logging
    private Logger logger  = LoggerFactory.getLogger(FileServiceImpl.class);

    @Override
    public String upload(MultipartFile file, String path){
        String fileName = file.getOriginalFilename();
        //Extension
        String fileExtensionName = fileName.substring(fileName.lastIndexOf('.')+1);
        //Use UUID to prevent duplicate file names and overwrite other people's files
        String uploadFileName = UUID.randomUUID().toString() + "." + fileExtensionName;
        logger.info("Start to upload the file. The name of the uploaded file:{}，Upload path:{}，New filename:{} ",fileName,path,uploadFileName);
        //new file
        File fileDir = new File(path);
        //Determine whether the file exists, and create a new one if it does not exist
        if(!fileDir.exists()){
            //Make the file modifiable, because after Tomcat publishes the service, the permissions of the file may not be modifiable
            fileDir.setWritable(true);
            //dirs is used to solve the problem that if a folder is not created in the uploaded path, it will automatically create a folder
            fileDir.mkdirs();
        }
        File targetFile = new File(path,uploadFileName);
        try {
            file.transferTo(targetFile);
            //So far, the file has been uploaded to the server successfully

            //The next step is to upload the file to the FTP server and connect with the FTP file server
            List <File> fileList = new ArrayList<>();
            FTPUtil.uploadFile(fileList);
            //File uploaded to FTP

            //After uploading, delete the file under upload
            targetFile.delete();

        } catch (IOException e) {
            logger.error("Upload file exception",e);
            return null;
        }

        return  targetFile.getName();

    }

}