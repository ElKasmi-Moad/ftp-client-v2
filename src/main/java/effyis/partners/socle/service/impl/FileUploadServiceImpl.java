package effyis.partners.socle.service.impl;


import effyis.partners.socle.configuration.FTPProperties;
import effyis.partners.socle.service.FileUploadService;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Service
public class FileUploadServiceImpl implements FileUploadService {



    protected FTPClient ftpClient;
    private FTPProperties FTPProperties;

    private final Path root = Paths.get("uploads");

    @Autowired
    public FileUploadServiceImpl(@Autowired FTPProperties FTPProperties) {
        this.FTPProperties = FTPProperties;
    }

    @PostConstruct
    public void init() throws IOException {
        if (this.FTPProperties.isAutoStart()) {

            this.open();
        }
    }

    @Override
    public boolean open() {
        close();
        ftpClient = new FTPClient();
        boolean loggedIn = false;
        try {
            ftpClient.connect(FTPProperties.getServer(), FTPProperties.getPort());
            loggedIn = ftpClient.login(FTPProperties.getUsername(), FTPProperties.getPassword());
            if (FTPProperties.getKeepAliveTimeout() > 0)
                ftpClient.setControlKeepAliveTimeout(FTPProperties.getKeepAliveTimeout());
            System.out.println("LOGGED IN");
        } catch (Exception e) {
           System.out.println(e);
        }
        return loggedIn;
    }

    public void close() {
        if (ftpClient != null) {
            try {
                ftpClient.logout();
                ftpClient.disconnect();
                System.out.println("Disconnecting");
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    public boolean loadFile(String remotePath, OutputStream outputStream) {
        try {

            return ftpClient.retrieveFile(remotePath, outputStream);
        } catch (IOException e) {

            return false;
        }
    }

    public boolean saveFile(InputStream inputStream, String destPath, boolean append) {
        try {

            if(append)
                return ftpClient.appendFile(destPath, inputStream);
            else
                System.out.println("TRYING TO FETCH THE FILE");
            return ftpClient.storeFile(destPath, inputStream);
        } catch (IOException e) {
            System.out.println("Shit happens");
            return false;
        }
    }
    public  String uploadOrder(String fileName)
    {

        String message ="Conn failed";
        try
        {
            //username and password for ftp server
            if (ftpClient.isConnected())
            {
                //passive mode - IMPORTANT
                ftpClient.enterLocalPassiveMode();
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);//only for txt file ACII mode, for rest binary mode
                // String data = Environment.getExternalStorageDirectory().getPath() +"/"+ fileName; //data location
                String data = "C:/Users/moad el kasmi/Desktop/" + fileName;
                InputStream in = new FileInputStream(data);
                File f = new File(data);

                if(ftpClient.storeFile("text", in))
                {
                    message = "Upload success!";
                }
                if(!f.exists()) message = "File not exists.";
                in.close();
                //logging out
                ftpClient.logout();
                ftpClient.disconnect();
            }
            else
            {

                message = "Unsuccessful you are not connected";
            }
        }
        catch (Exception e)
        {
            message = e.toString();
        }
        return message;
    }

    @Override
    public  String uploadFile(MultipartFile file)
    {

        String message ="Conn failed";
        try
        {
            //username and password for ftp server
            if (ftpClient.isConnected())
            {
                //passive mode - IMPORTANT
                ftpClient.enterLocalPassiveMode();
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);//only for txt file ACII mode, for rest binary mode
                // String data = Environment.getExternalStorageDirectory().getPath() +"/"+ fileName; //data location
               // String data = "C:/Users/moad el kasmi/AppData/Local/Postman/files/" + file.getOriginalFilename();
                InputStream in = file.getInputStream();
                if(ftpClient.storeFile(file.getOriginalFilename(), in))
                {
                    message = "Upload success!";
                }
                /*
                if(!file.isEmpty()) message = "File not exists.";
                in.close();
                //logging out
                ftpClient.logout();
                ftpClient.disconnect();

                 */
            }
            else
            {

                message = "Unsuccessful you are not connected";
            }
        }
        catch (Exception e)
        {
            message = e.toString();
        }
        return message;
    }


    @Override
    public String save(MultipartFile file) {
        String message = "";
        try {
            Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
            message = "Success";
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());


        }
        return  message;
    }



    public boolean saveFile(String sourcePath, String destPath, boolean append) {
        InputStream inputStream = null;
        try {
            inputStream = new ClassPathResource(sourcePath).getInputStream();
        } catch (IOException e) {
            return false;
        }
        return this.saveFile(inputStream, destPath, append);
    }

    public boolean isConnected() {
        boolean connected = false;
        if (ftpClient != null) {
            try {
                connected = ftpClient.sendNoOp();
            } catch (Exception e) {

            }
        }
        return connected;
    }



    @Override
    public String uploadOrder(MultipartFile file) {
        return null;
    }
}