package effyis.partners.socle.util;
/*
 *   @author ayoubbenyas
 */

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class CustomTime {
    public static LocalDateTime getTimeStamp() {
        Date input = new Date();
        return input.toInstant().atZone(ZoneId.of("Africa/Tunis")).toLocalDateTime();
    }
}
